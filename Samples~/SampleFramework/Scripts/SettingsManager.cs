﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

/// <summary>
/// Manager for the settings in the game.
/// </summary>
public class SettingsManager : MonoBehaviour
{
    private const string MasterVolumeKey = "MasterVolume";
    private const string MusicVolumeKey = "MusicVolume";
    private const string SfxVolumeKey = "SFXVolume";

    /// <summary>
    /// Main mixer for all audio.
    /// </summary>
    [SerializeField] private AudioMixer mainMixer;

    /// <summary>
    /// Master volume slider.
    /// </summary>
    [SerializeField] private Slider masterSlider;

    /// <summary>
    /// UI slider reference that controls the music volume.
    /// </summary>
    [SerializeField] private Slider musicSlider;

    /// <summary>
    /// UI slider reference that controls the sfx volume.
    /// </summary>
    [SerializeField] private Slider sfxSlider;

    private void OnEnable()
    {
        // Restore values from the player preferences file. If none exists,
        // use default starting values.
        masterSlider.value = PlayerPrefs.GetFloat(MasterVolumeKey, masterSlider.maxValue);
        musicSlider.value = PlayerPrefs.GetFloat(MusicVolumeKey, musicSlider.maxValue);
        sfxSlider.value = PlayerPrefs.GetFloat(SfxVolumeKey, sfxSlider.maxValue);
    }

    /// <summary>
    /// Callback when the master volume slider changes in the UI.
    /// </summary>
    public void OnMasterVolumeChanged(float value)
    {
        PlayerPrefs.SetFloat(MasterVolumeKey, value);
        mainMixer.SetFloat(MasterVolumeKey, ConvertToVolume(value));
    }

    /// <summary>
    /// Callback when the music volume slider has changed in the UI.
    /// </summary>
    public void OnMusicVolumeChanged(float value)
    {
        PlayerPrefs.SetFloat(MusicVolumeKey, value);
        mainMixer.SetFloat(MusicVolumeKey, ConvertToVolume(value));
    }

    /// <summary>
    /// Callback when the sfx slider has changed in the UI.
    /// </summary>
    public void OnSfxVolumeChanged(float value)
    {
        PlayerPrefs.SetFloat(SfxVolumeKey, value);
        mainMixer.SetFloat(SfxVolumeKey, ConvertToVolume(value));
    }

    /// <summary>
    /// Converts a value from 0.0001-1 to -80 to 0 for the volume value.
    /// </summary>
    /// <param name="value">Float between 0.0001 to 1.</param>
    /// <returns>A float between -80 to 0 representing the volume.</returns>
    private float ConvertToVolume(float value)
    {
        return Mathf.Log10(value) * 20;
    }
}
