# DACK Utilities Quick Guide
This custom package includes common used scripts and editor tools used for creating games in Unity. The scripts and tools included here have no external dependencies so it can be imported into any blank starting project without any issues. Below are the features and namespaces to access the utility scripts.

# DACK.Attributes
There are custom attributes for displaying the data better in the editor inspector window for serialized fields. Currently, there are two: `[Layer]` and `[ReadOnly]`.
- `[Layer]` allows a more user friendly way of picking layers from a drop down instead of bits.
- `[ReadOnly]` makes the field disabled so it allows values to be shown in the inspector for viewing without getting modified accidentally. Useful if you need to check a value during play without having to switch to debug mode in the inspector.

There is a `[OnChangedCall(<string method>)]` attribute that can be attached to serialized properties in Unity that allows the script to be able to respond to property changes made in the editor. Useful for debugging when changes to a property in play mode need to be reflected in the game/editor screen. An example of how this was used in another game was updating the duration of a set of particle systems, which would loop through all particles attached to the object to update the durations and lifetimes to reflect the new value.

# DACK.Audio
## AudioMusicPlayer
The music player is a background music player using two audio sources to play an intro and loop section of a music using `BackgroundAudioSO` as the asset. This is put on a GameObject with two audio sources and the script attached to it. The player responds to a `BackgroundMusicEvent` asset that specifies how the music will be played or whether to stop the current music.

## AudioSFXPlayer
The SFX player is a 2D one-shot audio clip player. It responds to the `AudioClipEvent` asset that specifies the audio clip to play.

# DACK.Common
## SerializableQuaternion/SerializableVector3
The common namespace includes a special Quaternion and Vector3 implementation that is serialiable if the data is needed to be serialized into a file or through a network packet. The default Unity Quaternion and Vector3 are not seriailable, so these are needed if that's required.

## GamePreferencesSO
Basic game preferences scriptable object used for keeping track of audio volumes. Used more as a sample.

# DACK.Event
The core eventing system used in a lot of DACK games. This version is a refactor from the previous eventing system and uses both UnityAction and UnityEvent depending on how the person wants to interact with the eventing. The events can be interfaced either through traditional C# eventing for more code based implementation or through Unity's property for drag/drop modification.

## GameEvent
All events are inherited from `GameEvent` with additional properties added for custom scenarios. Custom game events have the following structure using the `LoadSceneEvent` as an example:
```
/// <summary>
/// Event for loading new scene(s) for the game.
/// </summary>
[CreateAssetMenu(menuName = "DACK/GameEvent/LoadSceneEvent")]
public class LoadSceneEvent : GameEvent
{
    /// <summary>
    /// Scenes to load from this event.
    /// </summary>
    [Tooltip("Scenes to load from this event.")]
    [SerializeField] private GameSceneSO[] scenesToLoad;

    /// <summary>
    /// Whether or not to show a loading screen when loading the scenes.
    /// </summary>
    [Tooltip("Whether or not to show a loading screen when loading the scenes.")]
    [SerializeField] private bool showLoadingScreen;

    /// <summary>
    /// Packs the scene data into an <see cref="LoadSceneEventData"/>.
    /// </summary>
    /// <returns>An <see cref="LoadSceneEventData"/> for the event callback.</returns>
    public override IGameEventData GetGameEventData()
    {
        return new LoadSceneEventData
        {
            ScenesToLoad = scenesToLoad,
            ShowLoadingScreen = showLoadingScreen,
        };
    }
}
```

Note that this structure is optional. Technically, all game events can use the base `GameEvent` without any modifications and pass in custom `IGameEventData` objects when raising the event and it'll work fine. The purpose of inheriting and modifying the included properties and asset menu is to make it more clear what event is being sent, have more "event type" safety when assigning events in the Unity property window as assets, and allow in-game testing of events using the custom property window that appears when a game event asset is selected. `GetGameEventData` is called by the editor script when the button for firing the event is clicked on the property window. If not implemented, the button will send a null `IGameEventData` object.

## Subscribing to Events
There's two ways of subscribing to events and setting event callbacks with this eventing system.

### Unity Style using GameEventListener
This MonoBehaviour script is added to any object that wants to listen for events and use the Unity content driven way of assigning event callbacks through the property window. Add it to any game object and it will add `UnityEvents` to the object to allow responding to one `GameEvent`. Drag and drop events and functions to set the callbacks in the property window.

### C# Method of Eventing
The more C# method is to treat the game events as C# events and subscribe to them. The base `GameEvent` class has a field called

`UnityAction<IGameEventData> OnEventRaised`

which works the same as regular C# events. To subscribe/unsubscribe in the MonoBehaviour, use the following convention to prevent memory leaks:
```
public void OnEnable()
{
    // Direct event subscription.
    gameEvent.OnEventRaised += OnGameEvent;
    
    // Do the direct method or use the Subscribe function, but not both.
    gameEvent.Subscribe(OnGameEvent);

    // NOTE: This may not work if the game event isn't set to null explicitly as 
    // Unity overrides the equality for null checks on Unity objects, so this can fail.
    // If null checks are needed, use an explcit (gameEvent != null) to ensure it works.
    //gameEvent?.Subscribe(OnGameEvent);
}

public void OnDisable()
{
    // Direct event subscription
    gameEvent.OnEventRaised -= OnGameEvent;

    // Do the direct method or use the Unsubscribe function, but not both.
    gameEvent.Unsubscribe(OnGameEvent);

    // NOTE: Same as above, do this at your own risk. Do an explicit null check to
    // be sure this functions as expected.
    //gameEvent?.Unsubscribe(OnGameEvent);
}
```
In this way, the event is guaranteed to subscribe when the game object becomes active and properly removed if the game object is disabled or destroyed. This approach is more programmer oriented and less content driven.

The callback function handler is as follows:

```
void OnGameEvent(IGameEventData eventData)
{
    var castData = eventData as EventData<string>;
    ...
}
```
Cast the `IGameEventData` to the data type your are expecting and proceed from there.

Generic one variable event data can be sent using `EventData<T>` where `T` is the variable to send through the event and setting the `Value` property. In this way, a custom event data isn't needed for one variable data, but custom data is still needed for sending more than one variable.
```
gameEvent.Raise(new EventData<int> { Value = 1 });
```

# DACK.Event.Data
This namespace contains all the data classes that will be passed in through the `GameEvent` when using `GameEvent.Raise(data)`. These all inherit from `IGameEventData` and add their own C# properties.

# DACK.Extensions
Contains useful extensions to Unity and C# classes.
- `GameObjectExtension` allows searching for children with specific tags and returning the game object or transform of it, searching for components in children with a tag, or finding an game object with a specified name starting with a specified game object.
- `ListExtension` adds `PickRandom` and `PopRandom` or selecting a random item in the list as well as incrementing an index with wrap around to loop through a list.
- `SceneExtension` adds the ability to get a component from a specified scene. Can be useful for multi-scene projects and access to a component is needed.

# DACK.SceneManagement
The `SceneLoader` is meant to be in a base scene that's never unloaded from the game and subscribes to the `LoadSceneEvent` to help unload and load scenes in the game. It uses `GameSceneSO` scriptable objects to determine what scene to load from the `LoadSceneEvent`.

# DACK.SceneManagement.ScriptableObjects
This namespace contains the `GameSceneLibrarySO`, which is only used for networked games to help sync scenes by efficiently passing an integer instead of string content, and `GameSceneSO`, which contains the scene to load when used by the `LoadSceneEvent`. Note that there is a custom editor for the `GameSceneSO` that allows the user to select the scene from the dropdown menu to prevent errors if typing the scene names by hand. The slight downside is the scenes need to be added to the build in order to show up in the dropdown for selection.

There is also `MenuSO`, but this is currently unused.

# DACK.Variables
The variables here allow for either static constant numbers to be used in a property or linking the property to a scriptable object asset that stores either a float, integer, or string. These are used to create a separation between objects that may need to share information between each other, but doesn't create a dependency since the objects will be linked to the variable asset and not directly to each other. This is a good way to allow objects to share information through a content way instead of code and allows quick switching of information without the need to recompile scripts.

The primary use for these is for updating UI content as the UI and a game object script can be linked to the same variable and during the UI update cycle, it can automatically pull information from the asset without any knowledge of where it came from. This separates the UI from the rest of the game so it doesn't have any dependencies (other than the variable) and can be moved to different projects without dragging other dependencies with it.

Variables come in two parts: `<Data>Variable` and `<Data>Reference`. The variable part adds a custom menu to allow creating it as a scriptable asset while the reference part is used in scripts where it can be swapped between the variable content or a constant value in the property window. A custom editor script allows this to switched in the property window in a small dropdown menu.

## References
A `<Value>Reference` allows the object to be either a constant or a `Variable` by setting the `useConstant` field either in the property viewer or using the `UseConstant` C# property. The generic reference is declared with the value type and the `Variable` class representing the variable version. The `FloatReference`, `IntReference`, and `StringReference` are included in the package. Follow the format below to create additional references as needed and adding the custom property drawer so it will render the new types correctly.

```
[Serializable]
public class FloatReference : GenericReference<float, FloatVariable> { }

// Example of adding it to the custom propery drawer in Editor/ReferenceDrawers.cs
[CustomPropertyDrawer(typeof(FloatReference))]
public class FloatReferenceDrawer : BaseReferenceDrawer { }
```

## Ownership
Basic variables inherit from `BaseVariable<T>` which contains common elements like a `Value` property and setting/checking for the owner. The owner system allows the variables to be restricted to certain classes so observer classes won't accidentally change the values. The `FloatVariable`, `IntVariable`, and `StringVariable` contain helper methods `SetValue` and `ApplyChange` for setting values with an owner check. If no owner check is needed, the `Value` property can be changed directly.

`GenericReference` uses the ownership of the variable type if using a variable.

# DACK.Utility
Contains various helpful fuctions and extensions for math, world-to-UI conversion, and a layer extension for comparing layer mask bits.