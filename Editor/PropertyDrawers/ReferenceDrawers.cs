﻿using UnityEditor;

namespace DACK.Variables
{
    [CustomPropertyDrawer(typeof(StringReference))]
    public class StringReferenceDrawer : BaseReferenceDrawer { }

    [CustomPropertyDrawer(typeof(IntReference))]
    public class IntReferenceDrawer : BaseReferenceDrawer { }

    [CustomPropertyDrawer(typeof(FloatReference))]
    public class FloatReferenceDrawer : BaseReferenceDrawer { }
}