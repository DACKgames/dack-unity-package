﻿using UnityEditor;
using UnityEngine;

namespace DACK.Attributes
{
    /// <summary>
    /// Custom layout for displaying the layer names in an easy to read dropdown menu.
    /// </summary>
    [CustomPropertyDrawer(typeof(LayerAttribute))]
    class LayerAttributeEditor : PropertyDrawer
    {
        /// <summary>
        /// Creates the combo box on the property viewer in Unity for the layer selection.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="property"></param>
        /// <param name="label"></param>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            property.intValue = EditorGUI.LayerField(position, label, property.intValue);
        }
    }
}
