﻿// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// Modified by David Hsu.
// ----------------------------------------------------------------------------

using UnityEditor;
using UnityEngine;

namespace DACK.Event
{
    /// <summary>
    /// Custom editor for raising events while testing in the editor.
    /// </summary>
    [CustomEditor(typeof(GameEvent), true)]
    public class EventEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GUI.enabled = Application.isPlaying;

            GameEvent e = target as GameEvent;
            if (GUILayout.Button("Raise"))
            {
                e.Raise(e.GetGameEventData());
            }
        }
    }
}