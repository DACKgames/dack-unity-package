# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.9] - 2024-11-09
### Added
- Added FloatEvent

## [0.1.8] - 2024-02-07
### Added
- Added MergeMesh editor tools for merging meshes and exporting to OBJ
- Added OnChangeCallAttribute
- Added an Extensions folder to contain all extension classes
- Added GameObject, List, and Scene extensions
- Added a catch all Utils helper class
- Added WorldToUICoordinates function in ScreenSpaceHelper

### Changed
- Moved LayerExtensions into the Extensions folder

## [0.1.7] - 2022-12-08
### Added
- Added Subscribe/Unsubscribe to GameEvent
- Added ReadOnly attribute for read only fields in inspector

### Changed
- Made the naming for fading in/out more obvious in SceneLoader
- Simplified simple event data to use EventData<T> instead of an inherited class
- Changed variables to have an owner system
- Refactored references into a single GenericReference
- Refactored variables to inherit from a BaseVariable
- Rearranged property drawers to be in their own folder
- Changed editor assembly def to be Editor only and removed unnecessary defines
- Moved attributes out of editor folder and into the runtime in their own attributes folder
- Changed whitespace to spaces for SceneLoader

### Fixed
- Fixed SceneLoader to check if there's a scene loading before responding to the fade in completion
- Changing ownership will now only log if the new owner is different
- Fixed error when there's no main scenes to load

## [0.1.6] - 2022-04-23
### Fixed
- Corrected math error for normalizing the angle.

## [0.1.5] - 2022-04-23
### Added
- Utility folder with helpful math functions, World-to-UI calcuations, and layer extension.

## [0.1.4] - 2021-12-29
### Added
- New samples folder for boilerplate framework scene and events as well as audio settings and mixer

## [0.1.3] - 2021-11-18
### Changed
- SceneLoader can take a custom default animator property instead of being hard coded.
- SceneLoader change also removed playing an animation. The property will now need to cause a transition to an animation.
- LoadSceneEvent can pass in an bool animator property to set when loading a scene.

### Fixed
- Added C# event subscriptions to the AudioMusicPlayer and AudioGlobalSFXPlayer so they function correctly without having to manually add an game event listener component.

## [0.1.2] - 2021-11-06
### Changed
- Merged event data files into the event files to have less clutter
- Updated documentation to use LoadSceneEvent instead of BoolEvent since it has changed

## [0.1.1] - 2021-07-11
### Changed
- Simplified single value type events into a generic class that can be inherited without having to write boilerplate code for data

### Fixed
- Added editor checks to the editor files so projects can build

## [0.1.0] - 2021-04-14
### Added
- Initial import of the custom DACK Utilities package
- Full documentation about the namespaces and included functionality of the package

## [Unreleased]