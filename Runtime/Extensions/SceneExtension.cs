﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DACK.Extensions
{
    public static class SceneExtensions
    {
        /// <summary>
        /// Find a component in a specified scene.
        /// </summary>
        /// <typeparam name="T">Component to find.</typeparam>
        /// <param name="scene">The scene to search through for the component.</param>
        /// <param name="includeInactive">Set to true if inactive game objects should be included in the search. Default to false.</param>
        /// <returns>The first Unity component in the specified scene from the root. Null if none found.</returns>
        public static T GetComponent<T>(this Scene scene, bool includeInactive = false) where T : class
        {
            List<GameObject> roots = new List<GameObject>();
            scene.GetRootGameObjects(roots);

            T component = default;

            for (int i = 0, count = roots.Count; i < count; ++i)
            {
                component = roots[i].GetComponentInChildren<T>(includeInactive);
                if (component != null)
                    break;
            }

            return component;
        }
    }
}
