﻿using UnityEngine;

namespace DACK.Extensions
{
    public static class LayerExtension
    {
        /// <summary>
        /// Extension method to check if a layer is in a layermask. Default layer always returns true.
        /// </summary>
        /// <param name="mask"><see cref="LayerMask"/> bits to compare.</param>
        /// <param name="layer">Layer the object belongs to to check.</param>
        /// <returns>True if the layer is in the layer mask, false otherwise.</returns>
        public static bool Contains(this LayerMask mask, int layer)
        {
            return mask == (mask | (1 << layer));
        }

        /// <summary>
        /// Converts a layer mask to the first layer that's enabled.
        /// </summary>
        /// <param name="mask"><see cref="LayerMask"/> bits to check.</param>
        /// <returns>The integer layer that's on in the layer mask.</returns>
        public static int ToLayer(this LayerMask mask)
        {
            var bitmask = mask.value;

            UnityEngine.Assertions.Assert.IsFalse((bitmask & (bitmask - 1)) != 0,
                "MaskToLayer() was passed an invalid mask containing multiple layers.");

            int result = bitmask > 0 ? 0 : 31;
            while (bitmask > 1)
            {
                bitmask = bitmask >> 1;
                result++;
            }
            return result;
        }
    }
}