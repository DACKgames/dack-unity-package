﻿using System.Collections.Generic;
using UnityEngine;

namespace DACK.Extensions
{
    public static class ListExtension
    {
        /// <summary>
        /// Extension method to randomly choose and return an item in an enumerable list.
        /// </summary>
        /// <typeparam name="T">Type of object in the list.</typeparam>
        /// <param name="list"><see cref="List{T}"/> to choose from.</param>
        /// <returns>A random item from the list.</returns>
        public static T PickRandom<T>(this List<T> list)
        {
            if (list.Count == 0)
            {
                return default;
            }
            return list[Random.Range(0, list.Count)];
        }

        /// <summary>
        /// Extension method to randomly choose, remove, and return an item in an enumerable list.
        /// </summary>
        /// <typeparam name="T">Type of object in the list.</typeparam>
        /// <param name="list"><see cref="List{T}"/> to choose from.</param>
        /// <returns>A random item from the list.</returns>
        public static T PopRandom<T>(this List<T> list)
        {
            if (list.Count == 0)
            {
                return default;
            }

            int index = Random.Range(0, list.Count);
            T obj = list[index];
            list.RemoveAt(index);

            return obj;
        }

        /// <summary>
        /// Extension method to randomly choose and return an item in an array.
        /// </summary>
        /// <typeparam name="T">Type of object in the array.</typeparam>
        /// <param name="array"><see cref="T{}"/> to choose from.</param>
        /// <returns>A random item from the array.</returns>
        public static T PickRandom<T>(this T[] array)
        {
            if (array.Length == 0)
            {
                return default;
            }
            return array[Random.Range(0, array.Length)];
        }

        /// <summary>
        /// Extension method to get the next item index in the list given the current item, wrapping around if needed.
        /// </summary>
        /// <typeparam name="T">Type of object in the list.</typeparam>
        /// <param name="list"><see cref="List{T}"/> to choose from.</param>
        /// <param name="currentItem">The current item. The item must exist in the list and support Equals().</param>
        /// <returns>The index of the item next after the currentItem in the list. If currentItem is not found in list, returns the item in index 0.</returns>
        public static int GetNextIndexWithWrap<T>(this List<T> list, T currentItem)
        {
            int currentItemIndex = list.FindIndex(item => item.Equals(currentItem));
            if (currentItemIndex == -1)
            {
                return 0;
            }

            return GetNextIndexWithWrapInternal(currentItemIndex, list.Count - 1);

        }

        /// <summary>
        /// Extension method to get the next item index in the list given an index, wrapping around if needed.
        /// </summary>
        /// <typeparam name="T">Type of object in the list.</typeparam>
        /// <param name="list"><see cref="List{T}"/> to choose from.</param>
        /// <param name="currentItemIndex">The index to get next from.</param>
        /// <returns>The index of the item next after the currentItemIndex. If currentItemIndex is out of bounds, returns index 0.</returns>
        public static int GetNextIndexWithWrap<T>(this List<T> list, int currentItemIndex)
        {
            if (currentItemIndex < 0 || currentItemIndex >= list.Count)
            {
                return 0;
            }

            return GetNextIndexWithWrapInternal(currentItemIndex, list.Count - 1);
        }

        /// <summary>
        /// Extension method to get the previous item index in the list given the current item, wrapping around if needed.
        /// </summary>
        /// <typeparam name="T">Type of object in the list.</typeparam>
        /// <param name="list"><see cref="List{T}"/> to choose from.</param>
        /// <param name="currentItem">The current item. The item must exist in the list and support Equals().</param>
        /// <returns>The index of the item previous to the currentItem in the list. If currentItem is not found in list, returns the item in index 0.</returns>
        public static int GetPreviousIndexWithWrap<T>(this List<T> list, T currentItem)
        {
            int currentItemIndex = list.FindIndex(item => item.Equals(currentItem));
            if (currentItemIndex == -1)
            {
                return 0;
            }

            return GetPreviousIndexWithWrapInternal(currentItemIndex, list.Count - 1);
        }

                /// <summary>
        /// Extension method to get the previous item index in the list given the current item, wrapping around if needed.
        /// </summary>
        /// <typeparam name="T">Type of object in the list.</typeparam>
        /// <param name="list"><see cref="List{T}"/> to choose from.</param>
        /// <param name="currentItem">The index to get previous from.</param>
        /// <returns>The index of the item next after the currentItem in the list. If currentItemIndex is out of bounds, returns index 0.</returns>
        public static int GetPreviousIndexWithWrap<T>(this List<T> list, int currentItemIndex)
        {
            if (currentItemIndex < 0 || currentItemIndex >= list.Count)
            {
                return 0;
            }

            return GetPreviousIndexWithWrapInternal(currentItemIndex, list.Count - 1);
        }
        
        /// <summary>
        /// Extension method to choose the next item in the list given the current item, wrapping around if needed.
        /// </summary>
        /// <typeparam name="T">Type of object in the list.</typeparam>
        /// <param name="list"><see cref="List{T}"/> to choose from.</param>
        /// <param name="currentItem">The current item. The item must exist in the list and support Equals().</param>
        /// <returns>The item next after the currentItem in the list. If currentItem is not found in list, returns the item in index 0.</returns>
        public static T GetNextItemWithWrap<T>(this List<T> list, T currentItem)
        {
            int currentItemIndex = list.FindIndex(item => item.Equals(currentItem));
            if (currentItemIndex == -1)
            {
                return list[0];
            }

            return list[GetNextIndexWithWrapInternal(currentItemIndex, list.Count - 1)];
        }

        /// <summary>
        /// Extension method to choose the previous item in the list given the current item, wrapping around if needed.
        /// </summary>
        /// <typeparam name="T">Type of object in the list.</typeparam>
        /// <param name="list"><see cref="List{T}"/> to choose from.</param>
        /// <param name="currentItem">The current item. The item must exist in the list and support Equals().</param>
        /// <returns>The item in the index before currentItem in the list. If currentItem is not found in list, returns the item in index 0.</returns>
        public static T GetPreviousItemWithWrap<T>(this List<T> list, T currentItem)
        {
            int currentItemIndex = list.FindIndex(item => item.Equals(currentItem));
            if (currentItemIndex == -1)
            {
                return list[0];
            }

            return list[GetPreviousIndexWithWrapInternal(currentItemIndex, list.Count - 1)];
        }

        private static int GetNextIndexWithWrapInternal(int currentItemIndex, int maxIndex)
        {
            int nextIndex = (currentItemIndex >= maxIndex) ? 
                            0 : 
                            currentItemIndex + 1;
            return nextIndex;
        }

        private static int GetPreviousIndexWithWrapInternal(int currentItemIndex, int maxIndex)
        {
            int previousIndex = (currentItemIndex <= 0) ? 
                                        maxIndex : 
                                        currentItemIndex - 1;
            return previousIndex;
        }
    }
}
