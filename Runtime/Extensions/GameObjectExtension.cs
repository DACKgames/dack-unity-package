﻿using System.Linq;
using UnityEngine;

namespace DACK.Extensions
{
    /// <summary>
    /// Helpful methods with finding game objects within a game object.
    /// </summary>
    public static class GameObjectExtension
    {
        /// <summary>
        /// Extension method to find the first child <see cref="GameObject"/> with the specified tag by recursing through the object tree.
        /// </summary>
        /// <param name="go"><see cref="GameObject"/> to search from.</param>
        /// <param name="tag">Tag the object has to check.</param>
        /// <returns>The first <see cref="GameObject"/> child found with the matching tag. Null if nothing found.</returns>
        public static GameObject FindChildObjectWithTag(this GameObject go, string tag)
        {
            GameObject result = null;
            foreach (Transform child in go.transform)
            {
                if (child.CompareTag(tag))
                {
                    return child.gameObject;
                }

                // If there's additional children, recurse through
                if (child.childCount > 0)
                {
                    result = FindChildObjectWithTag(child.gameObject, tag);
                }

                if (result != null)
                {
                    return result;
                }
            }

            return result;
        }

        /// <summary>
        /// Extension method to find a <see cref="Transform"/> with the specified tag by recursing through the object tree.
        /// </summary>
        /// <param name="go"><see cref="GameObject"/> to search from.</param>
        /// <param name="tag">Tag the object has to check.</param>
        /// <returns>The first <see cref="Transform"/> child found with the matching tag. Null if nothing found.</returns>
        public static Transform FindChildTransformWithTag(this GameObject go, string tag)
        {
            Transform result = null;
            foreach (Transform child in go.transform)
            {
                if (child.CompareTag(tag))
                {
                    return child;
                }

                // If there's additional children, recurse through
                if (child.childCount > 0)
                {
                    result = FindChildTransformWithTag(child.gameObject, tag);
                }

                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }

        /// <summary>
        /// Extension method to find a <see cref="Transform"/> with the specified tag by recursing through the object tree.
        /// </summary>
        /// <param name="transform"><see cref="Transform"/> to search from.</param>
        /// <param name="tag">Tag the object has to check.</param>
        /// <returns>The first <see cref="Transform"/> child found with the matching tag. Null if nothing found.</returns>
        public static Transform FindChildTransformWithTag(this Transform transform, string tag)
        {
            return transform.gameObject.FindChildTransformWithTag(tag);
        }

        /// <summary>
        /// Extension method to find all components with the specified tag by recursing through the object tree.
        /// </summary>
        /// <typeparam name="T">Unity component to find in the children.</typeparam>
        /// <param name="go"><see cref="GameObject"/> to search from.</param>
        /// <param name="tag">Tag the object has to check.</param>
        /// <returns>Array of components in the children matching the tag.</returns>
        public static T[] FindComponentsInChildrenWithTag<T>(this GameObject go, string tag) where T : Component
        {
            return go.GetComponentsInChildren<T>().Where(x => x.tag == tag).ToArray();
        }

        /// <summary>
        /// Extension method to find the first game object with the specified name.
        /// </summary>
        /// <param name="go"><see cref="GameObject"/> to search from.</param>
        /// <param name="name">Name of the game object to find in the children.</param>
        /// <returns><see cref="GameObject"/> with the specified name, null if not found.</returns>
        public static GameObject FindObjectWithName(this GameObject go, string name)
        {
            if (go.name == name)
            {
                return go;
            }

            GameObject result = null;
            foreach (Transform child in go.transform)
            {
                if (child.name == name)
                {
                    return child.gameObject;
                }

                // If there's additional children, recurse through
                if (child.childCount > 0)
                {
                    result = FindObjectWithName(child.gameObject, name);
                }

                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }
    }
}
