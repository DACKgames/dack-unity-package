﻿using UnityEngine;

namespace DACK.Utility
{
    /// <summary>
    /// Global helper class for math related functions.
    /// </summary>
    public static class MathHelper
    {
        /// <summary>
        /// Clamps the given Quaternion's X-axis between the given minimum float and maximum float values.
        /// Returns the given value if it is within the min and max range.
        /// </summary>
        /// <param name="q">Target Quaternion.</param>
        /// <param name="minimum">Minimum float value.</param>
        /// <param name="maximum">Maximum float value.</param>
        /// <returns></returns>
        public static Quaternion ClampRotationAroundXAxis(Quaternion q, float minimum, float maximum)
        {
            q.x /= q.w;
            q.y = 0;
            q.z = 0;
            q.w = 1.0f;

            float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

            angleX = Mathf.Clamp(angleX, minimum, maximum);

            q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

            return q;
        }

        /// <summary>
        /// Normalize an angle between -180 and 180 degrees.
        /// </summary>
        /// <param name="a">Degree angle to normalize.</param>
        /// <returns>A normalized angle between -180 and 180.</returns>
        public static float NormalizeAngle(float a)
        {
            float angle = a - (Mathf.Floor(a / 360f) * 360f);
            
            if (angle > 180)
            {
                angle -= 360;
            }
            else if (angle < -180)
            {
                angle += 360;
            }

            return angle;
        }

        /// <summary>
        /// Clamps the provided angle between the min and max values normalized to -180 to 180 degrees.
        /// </summary>
        /// <param name="angle">Angle to clamp.</param>
        /// <param name="min">Minimum angle.</param>
        /// <param name="max">Maximum angle.</param>
        /// <returns>A normalized angle between -180 and 180 clamped to the provided min and max angles.</returns>
        public static float ClampAngle(float angle, float min, float max)
        {
            angle = NormalizeAngle(angle);
            return Mathf.Clamp(angle, min, max);
        }
    }
}
