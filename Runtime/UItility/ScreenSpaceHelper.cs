﻿using UnityEngine;

namespace DACK.Utility
{
    /// <summary>
    /// Global helper class for common functions converting world space to screen space.
    /// </summary>
    public static class ScreenSpaceHelper
    {
        /// <summary>
        /// Converts the world position of the player and the world position of another object into a 2D screen angle on the
        /// HUD to point the player toward the world position.
        /// </summary>
        /// <param name="playerTransform"><see cref="Transform"/> of the player object.</param>
        /// <param name="position"><see cref="Vector3"/> of a world position.</param>
        /// <returns>Angle in degrees between the player facing direction and the world position.</returns>
        public static float GetScreenAngle(Transform playerTransform, Vector3 position)
        {
            // Angle is negated because the rotation is reversed in the UI space compared to 3D
            var angle = -Mathf.Atan2(position.x - playerTransform.position.x, position.z - playerTransform.position.z) * Mathf.Rad2Deg;

            angle += MathHelper.NormalizeAngle(playerTransform.rotation.eulerAngles.y);

            return angle;
        }

        /// <summary>
        /// Converts a world position in 3D space into a 2D UI space.
        /// </summary>
        /// <param name="position"><see cref="Vector3"/> world position to transform.</param>
        /// <returns>A <see cref="Vector2"/> position in 2D UI space.</returns>
        public static Vector2 WorldToUICoordinates(Vector3 position)
        {
            return new Vector2(position.x, position.z);
        }

        /// <summary>
        /// Helper method to get the correct screen coordinates to render UI objects as the screen position returned by
        /// WorldToScreenPoint from the camera returns the position based on the set resolution of the canvas that's not scaled.
        /// This will take the scaled window into account and return the proper position in the window.
        /// </summary>
        /// <param name="cam">Camera used to get the screen position.</param>
        /// <param name="canvas">UI canvas where the element is parented under.</param>
        /// <param name="worldPos">World position to convert into screen space coordinates.</param>
        /// <returns>A <see cref="Vector2"/> screen space coordinate that accounts for the window size.</returns>
        public static Vector2 GetTrueScreenPosition(Camera cam, Canvas canvas, Vector3 worldPos)
        {
            var canvasRect = canvas.GetComponent<RectTransform>();
            var screenPos = cam.WorldToScreenPoint(worldPos);

            // Convert the screen position from the camera into the canvas resolution since the canvas resolution may not match
            // the screen resolution if the player changes it.
            Vector2 anchorPos = new Vector2(screenPos.x * canvasRect.sizeDelta.x / cam.pixelWidth,
                screenPos.y * canvasRect.sizeDelta.y / cam.pixelHeight);

            return anchorPos;
        }
    }
}
