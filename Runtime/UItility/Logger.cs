using System.Collections.Generic;
using UnityEngine;

namespace DACK.Utility
{
    /// <summary>
    /// General logging utility that can have several levels of logging.
    /// </summary>
    public static class Logger
    {
        /// <summary>
        /// Level of logging detail.
        /// </summary>
        public enum Level : int
        {
            INFO = 0,
            WARNING = 1,
            ERROR = 2,
            CRITICAL = 3,
        }

        private static HashSet<string> flags = new HashSet<string>();
        private static Level logLevel = Level.INFO;

        /// <summary>
        /// Sets the log level for displaying logging messages. All levels above the set
        /// level will show.
        /// </summary>
        /// <param name="newLevel">Lowest level to display the log messag.e</param>
        public static void SetLevel(Level newLevel)
        {
            logLevel = newLevel;
        }

        /// <summary>
        /// Adds a flag to display for logging.
        /// </summary>
        /// <param name="flag">String flag filter to add.</param>
        public static void EnableFlag(string flag)
        {
            flags.Add(flag);
        }

        /// <summary>
        /// Removes a flag to disable that log from displaying.
        /// </summary>
        /// <param name="flag">String flag filter to remove.</param>
        public static void DisableFlag(string flag)
        {
            if (flags.Contains(flag))
            {
                flags.Remove(flag);
            }
        }

        /// <summary>
        /// Logs a debug message.
        /// </summary>
        /// <param name="level"><see cref="Level"/> of this log message.</param>
        /// <param name="flag">String flag filter this log belongs to.</param>
        /// <param name="msg">The string message to log.</param>
        /// <param name="context">Optional object context for Unity to link against in the editor.</param>
        public static void Log(Level level, string flag, string msg, Object context = null)
        {
            if (level >= logLevel && flags.Contains(flag))
            { 
                if (level > Level.WARNING)
                {
                    Debug.LogError($"{flag}: {msg}", context);
                }
                else if (level > Level.INFO)
                {
                    Debug.LogWarning($"{flag}: {msg}", context);
                }
                else
                {
                    Debug.Log($"{flag}: {msg}", context);
                }
            }
        }
    }
}
