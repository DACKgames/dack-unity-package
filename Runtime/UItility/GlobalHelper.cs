﻿using System;
using System.Reflection;

namespace DACK.Utility
{
    /// <summary>
    /// Global utility function for getting common information not related to the game itself.
    /// </summary>
    public static class GlobalHelper
    {
        private static Version version = null;

        /// <summary>
        /// Gets the current version of the game.
        /// </summary>
        /// <returns>A string version.</returns>
        public static Version GetVersion()
        {
            if (version == null)
            {
                version = Assembly.GetAssembly(typeof(GlobalHelper)).GetName().Version;
            }

            return version;
        }

        /// <summary>
        /// Checks to see if the two version are compatible with each other. They are compatible if the major and minor
        /// are the same. The builds and revisions are minor enough that it doesn't matter.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>True if the versions are close enough, false otherwise.</returns>
        public static bool IsVersionCompatible(Version a, Version b)
        {
            return a.Major == b.Major &&
                a.Minor == b.Minor;
        }
    }
}
