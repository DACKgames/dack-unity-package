﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DACK.Utility
{
    /// <summary>
    /// General utility class for miscellaneous helper functions.
    /// </summary>
    public static class Utils
    {
        public static readonly RaycastDistanceCompare RaycastHitDistanceComparer = new RaycastDistanceCompare();

        /// <summary>
        /// Checks to see if the two version are compatible with each other. They are compatible if the major and minor
        /// are the same. The builds and revisions are minor enough that it doesn't matter.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>True if the versions are close enough, false otherwise.</returns>
        public static bool IsVersionCompatible(Version a, Version b)
        {
            return a.Major == b.Major &&
                a.Minor == b.Minor;
        }

        /// <summary>
        /// Custom comparer for raycast hit based on their distance.
        /// </summary>
        public class RaycastDistanceCompare : IComparer<RaycastHit>
        {
            public int Compare(RaycastHit x, RaycastHit y)
            {
                if (x.distance > y.distance) return 1;
                if (x.distance < y.distance) return -1;
                return 0;
            }
        }

        /// <summary>
        /// Changes all the game objects under the parent and the parent itself to the specified layer mask.
        /// </summary>
        /// <remarks>This only goes one layer deep. If all children are required, need to rewrite as recursive.</remarks>
        /// <param name="parent">Parent <see cref="Transform"/> of the children objects to change the layer mask.</param>
        /// <param name="layerMask">Layer mask to apply to all children.</param>
        public static void SetRenderLayerInChildren(Transform parent, LayerMask layerMask)
        {
            parent.gameObject.layer = layerMask;

            foreach (Transform child in parent)
            {
                child.gameObject.layer = layerMask;
            }
        }

        /// <summary>
        /// Converts a string scene name to the build index.
        /// </summary>
        /// <param name="sceneName">Scene to look up in the build settings</param>
        /// <returns>The index to the scene in the build settings if found, -1 if it doesn't exist.</returns>
        public static int SceneIndexFromName(string sceneName)
        {
            for (int i = 0; i < SceneManager.sceneCountInBuildSettings; i++)
            {
                // Get the full path from the build settings and parse out the scene name
                // NOTE: This assumes all scenes have unique filenames. If they have the
                // same name, but in different paths, then this will find the first one
                // in the build settings.
                string path = SceneUtility.GetScenePathByBuildIndex(i);
                string buildScene = Path.GetFileNameWithoutExtension(path);

                if (buildScene == sceneName)
                    return i;
            }

            return -1;
        }

        /// <summary>
        /// Helper method to find all objects that contain the interface and returns an enumrable of interface components.
        /// </summary>
        /// <typeparam name="T">Interface to find in the scene.</typeparam>
        /// <returns>An <see cref="IEnumerable{T}"/> of components matching the interface.</returns>
        public static IEnumerable<T> FindObjectsWithInterface<T>()
        {
            var interfaces = new List<T>();
            var rootGameObjects = SceneManager.GetActiveScene().GetRootGameObjects();

            foreach (var rootGameObject in rootGameObjects)
            {
                T[] childrenInterfaces = rootGameObject.GetComponentsInChildren<T>();
                foreach (var childInterface in childrenInterfaces)
                {
                    interfaces.Add(childInterface);
                }
            }

            return interfaces;
        }

        /// <summary>
        /// Finds all the children in the object with the specified tag.
        /// </summary>
        /// <typeparam name="T">Type to return for the children.</typeparam>
        /// <param name="parent">Parent game object to start the search from.</param>
        /// <param name="tag">Tag to look for.</param>
        /// <param name="includeInactive">True to include inactive objects.</param>
        /// <returns>An array of <typeparamref name="T"/> components.</returns>
        public static T[] FindComponentsInChildrenWithTag<T>(GameObject parent, string tag, bool includeInactive = false) where T : Component
        {
            if (parent == null) { throw new System.ArgumentNullException(); }
            if (string.IsNullOrEmpty(tag) == true) { throw new System.ArgumentNullException(); }
            List<T> list = new List<T>(parent.GetComponentsInChildren<T>(includeInactive));
            if (list.Count == 0) { return null; }

            for (int i = list.Count - 1; i >= 0; i--)
            {
                if (list[i].CompareTag(tag) == false)
                {
                    list.RemoveAt(i);
                }
            }
            return list.ToArray();
        }

        /// <summary>
        /// Finds the first child in the object with the specified tag.
        /// </summary>
        /// <typeparam name="T">Type to return for the children.</typeparam>
        /// <param name="parent">Parent game object to start the search from.</param>
        /// <param name="tag">Tag to look for.</param>
        /// <param name="includeInactive">True to include inactive objects.</param>
        /// <returns><typeparamref name="T"/> component with the tag.</returns>
        public static T FindComponentInChildWithTag<T>(GameObject parent, string tag, bool includeInactive = false) where T : Component
        {
            if (parent == null) { throw new System.ArgumentNullException(); }
            if (string.IsNullOrEmpty(tag) == true) { throw new System.ArgumentNullException(); }

            T[] list = parent.GetComponentsInChildren<T>(includeInactive);
            foreach (T t in list)
            {
                if (t.CompareTag(tag) == true)
                {
                    return t;
                }
            }
            return null;
        }

        /// <summary>
        /// Creates a new color using the base color and changing only the alpha of the color. Useful for changing default colors.
        /// </summary>
        /// <param name="baseColor"><see cref="Color"/> to change the alpha.</param>
        /// <param name="alpha">New alpha value to apply to the color.</param>
        /// <returns><see cref="Color"/> provided with only the alpha updated.</returns>
        public static Color TransparentColor(Color baseColor, float alpha)
        {
            baseColor.a = alpha;
            return baseColor;
        }

        /// <summary>
        /// Creates a new <see cref="Vector3"/> with randomized xyz values bassed on the min and max values.
        /// </summary>
        /// <param name="minValue">Minimum value for the vector.</param>
        /// <param name="maxValue">Maximum value for the vector.</param>
        /// <returns>A randomized <see cref="Vector3"/> with values in xyz between the min and max values.</returns>
        public static Vector3 RandomVector3(float minValue, float maxValue)
        {
            return new Vector3(UnityEngine.Random.Range(minValue, maxValue),
                UnityEngine.Random.Range(minValue, maxValue),
                UnityEngine.Random.Range(minValue, maxValue));
        }
    }
}
