﻿using DACK.Common;
using DACK.Event;
using DACK.SceneManagement.ScriptableObjects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace DACK.SceneManagement
{
    /// <summary>
    /// This class manages the scenes loading and unloading
    /// </summary>
    public class SceneLoader : MonoBehaviour
    {
        /// <summary>
        /// The animation key used for special effects on the loading screen.
        /// </summary>
        public const string DefaultLoadingAnimatorProperty = "Loading";

        [Header("Initialization Scene")]
        [Tooltip("Framwork scene that will persist throughout the game.")]
        [SerializeField] private GameSceneSO initializationScene = default;

        [Header("Load on start")]
        [Tooltip("Initial scenes that will load when the game begins.")]
        [SerializeField] private GameSceneSO[] mainScenes = default;

        [Header("Animations")]
        [Tooltip("Default bool animator property used for special effects on the loading screen.")]
        [SerializeField] private string loadingAnimatorProperty = DefaultLoadingAnimatorProperty;

        [Header("Loading Screen")]
        [SerializeField] private GameObject loadingInterface = default;
        [SerializeField] private Image loadingProgressBar = default;

        [Header("Load Event")]
        [Tooltip("Event to listen to for loading new scenes.")]
        [SerializeField] private LoadSceneEvent loadEventChannel = default;

        [Tooltip("Fade event when the screen fades to black.")]
        [SerializeField] private GameEvent fadeToBlackEvent;

        [Tooltip("Event fired when loading is complete.")]
        [SerializeField] private GameEvent loadCompleteEvent;

        [Tooltip("Event callback to immediately quit the game.")]
        [SerializeField] private GameEvent quitGameEvent;

        // List of the scenes to load and track progress
        private List<AsyncOperation> scenesToLoadAsyncOperations = new List<AsyncOperation>();
        private List<AsyncOperation> scenesToUnloadAsyncOperations = new List<AsyncOperation>();
        // List of scenes to unload/load
        private List<Scene> scenesToUnload = new List<Scene>();
        private List<string> scenesToLoad = new List<string>();
        // Keep track of the scene we want to set as active (for lighting/skybox)
        private GameSceneSO activeScene;

        // Buffer for the event message to keep track while waiting for an event callback
        // if showing a loading screen.
        private LoadSceneEventData sceneEventData;

        private bool useSynchronousLoading = false;
        private bool useLoadingScreen = false;
        private bool isLoadingScene = false;

        private void OnEnable()
        {
            loadEventChannel.OnEventRaised += OnLoadEventRaised;
            fadeToBlackEvent.OnEventRaised += OnFadeInComplete;
            quitGameEvent.OnEventRaised += OnQuit;
        }

        private void OnDisable()
        {
            loadEventChannel.OnEventRaised -= OnLoadEventRaised;
            fadeToBlackEvent.OnEventRaised -= OnFadeInComplete;
            quitGameEvent.OnEventRaised -= OnQuit;
        }

        private void Start()
        {
            if (SceneManager.GetActiveScene().name == initializationScene.sceneName)
            {
                LoadMainScenes();
            }
        }

        /// <summary>
        /// Handles the loading event when triggered.
        /// </summary>
        /// <param name="data"><see cref="LoadSceneEventData"/> information.</param>
        private void OnLoadEventRaised(IGameEventData data)
        {
            sceneEventData = data as LoadSceneEventData;

            if (sceneEventData == null)
            {
                Utility.Logger.Log(Utility.Logger.Level.WARNING,
                    DACKConstants.PackageFlag,
                    $"Incorrect data type ({data.GetType()}) in LocationLoader, expecting LoadSceneEventData");
                return;
            }

            // If we're showing the loading screen, then play the fade out animation and
            // handle unloading/loading after the fade out.
            if (sceneEventData.ShowLoadingScreen && loadingInterface != null)
            {
                // Show the progress bar and track progress if loadScreen is true
                loadingInterface.SetActive(true);
                isLoadingScene = true;

                // If there's an animator associated with the screen, then play the loading animation
                var animator = loadingInterface.GetComponent<Animator>();
                if (animator != null)
                {
                    if (!string.IsNullOrWhiteSpace(sceneEventData.AnimatorProperty))
                    {
                        animator.SetBool(sceneEventData.AnimatorProperty, true);
                    }
                    else
                    {
                        animator.SetBool(loadingAnimatorProperty, true);
                    }
                }
            }
            else
            {
                // If no loading screen, load the scene without any transition effects
                LoadScenes(sceneEventData.ScenesToLoad, false);
            }
        }

        /// <summary>
        /// Callback when the screen finishes fading to black when transitioning to a new scene.
        /// </summary>
        /// <param name="data">Not used.</param>
        private void OnFadeInComplete(IGameEventData data)
        {
            // It's possible the game is using the fade to black for a different purpose (cinematic effect, etc.) and not loading
            // a level, so if this detects the screen has finished fading to black, check to see if it's supposed to load a level
            // before attempting to do so.
            if (isLoadingScene)
            {
                LoadScenes(sceneEventData.ScenesToLoad, sceneEventData.ShowLoadingScreen);
            }
        }

        /// <summary>
        /// Callback when the scenes finish unloading.
        /// </summary>
        /// <param name="asyncOp">Unused.</param>
        private void OnUnloadFinished(AsyncOperation asyncOp)
        {
            // If we're synchronously unloading/loading, then start loading the new scenes at this point.
            // Otherwise, this callback does nothing.
            if (useSynchronousLoading)
            {
                LoadSceneHelper();
            }
        }

        private void LoadMainScenes()
        {
            if (mainScenes.Length > 0)
            {
                LoadScenes(mainScenes, false);
            }
        }

        /// <summary>
        /// This function loads the scenes passed as array parameter.
        /// </summary>
        private void LoadScenes(GameSceneSO[] locationsToLoad, bool showLoadingScreen)
        {
            // Add all current open scenes to unload list
            AddScenesToUnload();

            activeScene = locationsToLoad[0];
            useLoadingScreen = showLoadingScreen;
            useSynchronousLoading = locationsToLoad.Any(x => CheckLoadState(x.sceneName) == true);

            AddScenesToLoad(locationsToLoad);

            if (!useSynchronousLoading)
            {
                LoadSceneHelper();
            }

            // Unload the scenes in the unload stack
            UnloadScenes();

            isLoadingScene = false;
        }

        /// <summary>
        /// Callback when unloading/loading has finished. Expected to hit twice, once for loading and once
        /// for unloading, for it to considered complete.
        /// </summary>
        /// <param name="asyncOp"></param>
        private void SetActiveScene(AsyncOperation asyncOp)
        {
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(activeScene.sceneName));

            // Hide progress bar when loading is done
            // If there's an animator associated with the screen, then play the finished animation
            // Otherwise, disable the screen and immediately show the new scene.
            if (sceneEventData != null && sceneEventData.ShowLoadingScreen)
            {
                var animator = loadingInterface?.GetComponent<Animator>();
                if (animator != null)
                {
                    if (!string.IsNullOrWhiteSpace(sceneEventData.AnimatorProperty))
                    {
                        animator.SetBool(sceneEventData.AnimatorProperty, false);
                    }
                    else
                    {
                        animator.SetBool(DefaultLoadingAnimatorProperty, false);
                    }
                }
                else
                {
                    loadingInterface.SetActive(false);
                }
            }

            loadCompleteEvent?.Raise();
        }

        /// <summary>
        /// Helper method to add all the scenes needed to load.
        /// </summary>
        private void LoadSceneHelper()
        {
            foreach(var sceneName in scenesToLoad)
            {
                if (!CheckLoadState(sceneName))
                {
                    // Add the scene to the list of scenes to load asynchronously in the background
                    scenesToLoadAsyncOperations.Add(SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive));
                }
            }

            scenesToLoadAsyncOperations[0].completed += SetActiveScene;

            if (useLoadingScreen)
            {
                StartCoroutine(TrackLoadingProgress());
            }
            else
            {
                // Clear the scenes to load
                scenesToLoadAsyncOperations.Clear();
            }

            scenesToLoad.Clear();
        }

        private void AddScenesToLoad(GameSceneSO[] locationsToLoad)
        {
            for (int i = 0; i < locationsToLoad.Length; ++i)
            {
                string currentSceneName = locationsToLoad[i].sceneName;

                Utility.Logger.Log(Utility.Logger.Level.INFO,
                    DACKConstants.PackageFlag,
                    $"Added scene to load = {currentSceneName}");

                scenesToLoad.Add(currentSceneName);
            }
        }

        private void AddScenesToUnload()
        {
            for (int i = 0; i < SceneManager.sceneCount; ++i)
            {
                Scene scene = SceneManager.GetSceneAt(i);
                if (scene.name != initializationScene.sceneName)
                {
                    Utility.Logger.Log(Utility.Logger.Level.INFO,
                        DACKConstants.PackageFlag, 
                        $"Added scene to unload = {scene.name}");

                    // Add the scene to the list of the scenes to unload
                    scenesToUnload.Add(scene);
                }
            }
        }

        private void UnloadScenes()
        {
            if (scenesToUnload != null && scenesToUnload.Count > 0)
            {
                for (int i = 0; i < scenesToUnload.Count; ++i)
                {
                    // Unload the scene asynchronously in the background
                    scenesToUnloadAsyncOperations.Add(SceneManager.UnloadSceneAsync(scenesToUnload[i]));
                }

                scenesToUnloadAsyncOperations[0].completed += OnUnloadFinished;
                scenesToUnloadAsyncOperations.Clear();
            }

            scenesToUnload.Clear();
        }

        /// <summary>
        /// This function checks if a scene is already loaded.
        /// </summary>
        private bool CheckLoadState(String sceneName)
        {
            for (int i = 0; i < SceneManager.sceneCount; ++i)
            {
                Scene scene = SceneManager.GetSceneAt(i);
                if (scene.name == sceneName)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// This function updates the loading progress once per frame until loading is complete.
        /// </summary>
        private IEnumerator TrackLoadingProgress()
        {
            float totalProgress = 0;
            // When the scene reaches 0.9f, it means that it is loaded
            // The remaining 0.1f are for the integration
            while (totalProgress <= 0.9f)
            {
                // Reset the progress for the new values
                totalProgress = 0;

                // Iterate through all the scenes to load
                for (int i = 0; i < scenesToLoadAsyncOperations.Count; ++i)
                {
                    Utility.Logger.Log(Utility.Logger.Level.INFO,
                        DACKConstants.PackageFlag, 
                        $"Scene {i}: {scenesToLoadAsyncOperations[i].isDone} progress = {scenesToLoadAsyncOperations[i].progress}");

                    // Adding the scene progress to the total progress
                    totalProgress += scenesToLoadAsyncOperations[i].progress;
                }

                // The fillAmount for all scenes, so we devide the progress by the number of scenes to load
                if (loadingProgressBar != null)
                {
                    loadingProgressBar.fillAmount = totalProgress / scenesToLoadAsyncOperations.Count;
                    Utility.Logger.Log(Utility.Logger.Level.INFO,
                        DACKConstants.PackageFlag, 
                        $"progress bar {loadingProgressBar.fillAmount} and value = {totalProgress / scenesToLoadAsyncOperations.Count}");
                }

                yield return null;
            }

            // Clear the scenes to load
            scenesToLoadAsyncOperations.Clear();
        }

        /// <summary>
        /// Event callback when the quit event is raised, exiting the game immediately.
        /// </summary>
        /// <param name="data">Unused.</param>
        private void OnQuit(IGameEventData data)
        {
            Application.Quit();
            Utility.Logger.Log(Utility.Logger.Level.INFO,
                    DACKConstants.PackageFlag,
                    "Exit!");
        }
    }
}
