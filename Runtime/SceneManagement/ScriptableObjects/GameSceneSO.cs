﻿using UnityEngine;

namespace DACK.SceneManagement.ScriptableObjects
{
	/// <summary>
	/// This class is a base class which contains what is common to all game scenes (Locations or Menus)
	/// </summary>
	[CreateAssetMenu(fileName = "GameScene", menuName = "DACK/Scene Data/Game Scene")]
	public class GameSceneSO : ScriptableObject
	{
		[Header("Information")]
		public string sceneName;
		public string shortDescription;
	}
}