﻿using UnityEngine;

namespace DACK.SceneManagement.ScriptableObjects
{
	public enum Menu
	{
		MainMenu,
		PauseMenu,
		PlayerSelectMenu,
	}

	/// <summary>
	/// This class contains Settings specific to Menus only
	/// </summary>
	[CreateAssetMenu(fileName = "NewMenu", menuName = "DACK/Scene Data/Menu")]
	public class MenuSO : GameSceneSO
	{
		[Header("Menu specific")]
		public Menu MenuType;
	}
}