﻿using UnityEngine;

namespace DACK.SceneManagement.ScriptableObjects
{
    /// <summary>
    /// Asset containing a list of all scenes in the game, which will be used by the network call to sync
    /// everyone's scene when transitioning. Only used if using the scene loader with multiplayer networking.
    /// </summary>
    [CreateAssetMenu(menuName = "DACK/SceneManagement/GameSceneLibrary")]
    public class GameSceneLibrarySO : ScriptableObject
    {
        /// <summary>
        /// List of all game scenes used in this game.
        /// </summary>
        [SerializeField] private GameSceneSO[] gameScenes;

        /// <summary>
        /// Helper method to get the index of the game scene asset in the library.
        /// </summary>
        /// <param name="gameScene"><see cref="GameSceneSO"/> to find in the library.</param>
        /// <returns>The index of the scene in the library, -1 if it doesn't exist.</returns>
        public int GetIndex(GameSceneSO gameScene)
        {
            for(int i = 0; i < gameScenes.Length; i++)
            {
                if (gameScenes[i] == gameScene)
                {
                    return i;
                }
            }

            return -1;
        }
    }
}
