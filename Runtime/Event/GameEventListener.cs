﻿// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// Modified by David Hsu
// ----------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.Events;

namespace DACK.Event
{
    /// <summary>
    /// General purpose event listener that allows modification of the event response through the inspector when responding
    /// to a single event.
    /// </summary>
    public class GameEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        [SerializeField]
        protected GameEvent gameEvent;

        [Tooltip("Response to invoke when Event is raised.")]
        [SerializeField]
        protected UnityEvent<IGameEventData> gameResponse;

        private void OnEnable()
        {
            gameEvent.OnEventRaised += OnEventRaised;
        }

        private void OnDisable()
        {
            gameEvent.OnEventRaised -= OnEventRaised;
        }

        public void OnEventRaised(IGameEventData data)
        {
            gameResponse.Invoke(data);
        }
    }
}