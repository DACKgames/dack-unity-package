﻿using UnityEngine;

namespace DACK.Event
{
    /// <summary>
    /// Event sent when it needs to pass only a bool.
    /// </summary>
    [CreateAssetMenu(menuName = "DACK/GameEvent/BoolEvent")]
    public class BoolEvent : SimpleEvent<bool> { }
}
