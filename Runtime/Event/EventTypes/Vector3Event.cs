﻿using UnityEngine;

namespace DACK.Event
{
    /// <summary>
    /// Event for sending a Vector3.
    /// </summary>
    [CreateAssetMenu(menuName = "DACK/GameEvent/Vector3Event")]
    public class Vector3Event : SimpleEvent<Vector3> { }
}
