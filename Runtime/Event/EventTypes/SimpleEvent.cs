﻿using System;
using UnityEngine;

namespace DACK.Event
{
    /// <summary>
    /// Data for the simple event that contains only one value.
    /// </summary>
    [Serializable]
    public class EventData<T> : IGameEventData
    {
        /// <summary>
        /// <see cref="T"/> Value for this event.
        /// </summary>
        public T Value { get; set; }
    }

    /// <summary>
    /// Event for sending a single value.
    /// </summary>
    /// <typeparam name="T">The data type to pass in the event.</typeparam>
    public class SimpleEvent<T> : GameEvent
    {
        /// <summary>
        /// Value <see cref="T"/> to pass in the event.
        /// </summary>
        [Tooltip("Value to pass in the event.")]
        [SerializeField] protected T value;

        /// <summary>
        /// Raise has a type check to ensure the simple event is sending the data it's supposed to be.
        /// </summary>
        /// <param name="data"><see cref="IGameEventData"/> object with information for the event.</param>
        /// <param name="componentName">Optional component name this event is called from for debugging.</param>
        /// <param name="assetName">Optional asset name of the event that's raising the event for debugging.</param>
        public override void Raise(IGameEventData data, string componentName = null, string assetName = null)
        {
            if (data as EventData<T> == null)
            {
                Debug.LogError($"{name} Attempting to send in correct data type {data.GetType()}, expecting {typeof(EventData<T>).ToString()}");
                return;
            }

            base.Raise(data, componentName, assetName);
        }

        /// <summary>
        /// Packs data into <see cref="EventData{T}"/>.
        /// </summary>
        /// <returns>An <see cref="EventData{T}"/> for the event callback.</returns>
        public override IGameEventData GetGameEventData()
        {
            return new EventData<T>
            {
                Value = value,
            };
        }
    }
}
