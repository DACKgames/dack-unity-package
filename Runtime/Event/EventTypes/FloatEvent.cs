﻿using UnityEngine;

namespace DACK.Event
{
    /// <summary>
    /// Event sent when it needs to pass only a float.
    /// </summary>
    [CreateAssetMenu(menuName = "DACK/GameEvent/FloatEvent")]
    public class FloatEvent : SimpleEvent<float> { }
}
