﻿using System;
using UnityEngine;

namespace DACK.Event
{
    /// <summary>
    /// Custom data for telling how to play the background music.
    /// </summary>
    [Serializable]
    public class BackgroundMusicEventData : IGameEventData
    {
        /// <summary>
        /// True if the music will be fading in or out.
        /// </summary>
        public bool UseFade { get; set; } = false;

        /// <summary>
        /// True if the music will start, false if stopping the music. If UseFade is
        /// set to true, the music will do the fade in. Otherwise, this will immediately
        /// start or stop the music.
        /// </summary>
        public bool StartMusic { get; set; } = false;
    }

    /// <summary>
    /// Special event for telling background music to play.
    /// </summary>
    [CreateAssetMenu(menuName = "DACK/GameEvent/BackgroundMusicEvent")]
    public class BackgroundMusicEvent : GameEvent
    {
        /// <summary>
        /// True if the music will be fading in or out.
        /// </summary>
        public bool useFade;

        /// <summary>
        /// True if the music will start, false if stopping the music. If UseFade is
        /// set to true, the music will do the fade in. Otherwise, this will immediately
        /// start or stop the music.
        /// </summary>
        public bool startMusic;

        /// <summary>
        /// Packs the audio clip into an <see cref="AudioClipEventData"/>.
        /// </summary>
        /// <returns>An <see cref="AudioClipEventData"/> for the event callback.</returns>
        public override IGameEventData GetGameEventData()
        {
            return new BackgroundMusicEventData
            {
                UseFade = useFade,
                StartMusic = startMusic,
            };
        }
    }
}
