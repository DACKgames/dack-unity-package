﻿using UnityEngine;

namespace DACK.Event
{
    /// <summary>
    /// Event for sending a Vector2.
    /// </summary>
    [CreateAssetMenu(menuName = "DACK/GameEvent/Vector2Event")]
    public class Vector2Event : SimpleEvent<Vector2> { }
}
