﻿using UnityEngine;

namespace DACK.Event
{
    /// <summary>
    /// Event sent when it needs to pass only an integer.
    /// </summary>
    [CreateAssetMenu(menuName = "DACK/GameEvent/IntegerEvent")]
    public class IntegerEvent : SimpleEvent<int> { }
}
