﻿using UnityEngine;

namespace DACK.Event
{
    /// <summary>
    /// Triggers a dialog to show up with the specified text.
    /// </summary>
    [CreateAssetMenu(menuName = "DACK/GameEvent/StringEvent")]
    public class StringEvent : SimpleEvent<string> { }
}
