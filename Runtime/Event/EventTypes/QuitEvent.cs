﻿using UnityEngine;

namespace DACK.Event
{
    /// <summary>
    /// Event that will immediately exit the game when raised.
    /// </summary>
    /// <remarks>Only one should exist.</remarks>
    [CreateAssetMenu(menuName = "DACK/GameEvent/QuitEvent")]
    public class QuitEvent : GameEvent
    {
        /// <summary>
        /// Quit the application when raised.
        /// </summary>
        /// <param name="componentName">Optional component name this event is called from for debugging.</param>
        /// <param name="assetName">Optional asset name of the event that's raising the event for debugging.</param>
        public override void Raise(string componentName = null, string assetName = null)
        {
            Application.Quit();
        }
    }
}
