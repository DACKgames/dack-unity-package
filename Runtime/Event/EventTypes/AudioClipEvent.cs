﻿using UnityEngine;

namespace DACK.Event
{
    /// <summary>
    /// Special event for sending a clip to play to an audio source at runtime.
    /// </summary>
    [CreateAssetMenu(menuName = "DACK/GameEvent/AudioClipEvent")]
    public class AudioClipEvent : SimpleEvent<AudioClip> { }
}
