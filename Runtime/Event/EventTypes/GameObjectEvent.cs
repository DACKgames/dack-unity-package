﻿using UnityEngine;

namespace DACK.Event
{
    /// <summary>
    /// Event sent when it needs to pass only a game object.
    /// </summary>
    [CreateAssetMenu(menuName = "DACK/GameEvent/GameObjectEvent")]
    public class GameObjectEvent : SimpleEvent<GameObject> { }
}
