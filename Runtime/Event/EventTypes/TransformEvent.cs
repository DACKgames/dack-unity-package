﻿using UnityEngine;

namespace DACK.Event
{
    /// <summary>
    /// Event for sending a single value.
    /// </summary>
    [CreateAssetMenu(menuName = "DACK/GameEvent/TransformEvent")]
    public class TransformEvent : SimpleEvent<Transform> { }
}
