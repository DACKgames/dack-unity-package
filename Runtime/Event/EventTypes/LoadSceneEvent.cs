﻿using DACK.SceneManagement.ScriptableObjects;
using System;
using UnityEngine;

namespace DACK.Event
{
    /// <summary>
    /// Transmits the scenes to load and whether to show a loading screen.
    /// </summary>
    [Serializable]
    public class LoadSceneEventData : IGameEventData
    {
        /// <summary>
        /// Scenes to load.
        /// </summary>
        public GameSceneSO[] ScenesToLoad { get; set; }

        /// <summary>
        /// Whether or not to show a loading screen when transitioning.
        /// </summary>
        public bool ShowLoadingScreen { get; set; }

        /// <summary>
        /// Bool property to set in the animator to use when transitioning scenes and show loading screen is true.
        /// </summary>
        public string AnimatorProperty { get; set; }
    }

    /// <summary>
    /// Event for loading new scene(s) for the game.
    /// </summary>
    [CreateAssetMenu(menuName = "DACK/GameEvent/LoadSceneEvent")]
    public class LoadSceneEvent : GameEvent
    {
        /// <summary>
        /// Scenes to load from this event.
        /// </summary>
        [Tooltip("Scenes to load from this event.")]
        [SerializeField] private GameSceneSO[] scenesToLoad;

        /// <summary>
        /// Whether or not to show a loading screen when loading the scenes.
        /// </summary>
        [Tooltip("Whether or not to show a loading screen when loading the scenes.")]
        [SerializeField] private bool showLoadingScreen;

        [Tooltip("Bool property to set in the animator to use when transitioning scenes and show loading screen is true.")]
        [SerializeField] private string animatorProperty;

        /// <summary>
        /// Packs the scene data into an <see cref="LoadSceneEventData"/>.
        /// </summary>
        /// <returns>An <see cref="LoadSceneEventData"/> for the event callback.</returns>
        public override IGameEventData GetGameEventData()
        {
            return new LoadSceneEventData
            {
                ScenesToLoad = scenesToLoad,
                ShowLoadingScreen = showLoadingScreen,
                AnimatorProperty = animatorProperty,
            };
        }
    }
}