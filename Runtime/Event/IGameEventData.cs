﻿namespace DACK.Event
{
    /// <summary>
    /// Generic interface used to tie in different game events for casting. Base interface does nothing.
    /// </summary>
    public interface IGameEventData { }
}
