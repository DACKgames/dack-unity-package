﻿// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// Modified by David Hsu
// ----------------------------------------------------------------------------

using DACK.Common;
using UnityEngine;
using UnityEngine.Events;

namespace DACK.Event
{
    [CreateAssetMenu(menuName = "DACK/GameEvent/SimpleGameEvent")]
    public class GameEvent : ScriptableObject
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        public UnityAction<IGameEventData> OnEventRaised;

        /// <summary>
        /// Raises a basic event with no parameters.
        /// </summary>
        /// <param name="componentName">Optional component name this event is called from for debugging.</param>
        /// <param name="assetName">Optional asset name of the event that's raising the event for debugging.</param>
        public virtual void Raise(string componentName = null, string assetName = null)
        {
            if (OnEventRaised == null)
            {
                Utility.Logger.Log(Utility.Logger.Level.WARNING,
                    DACKConstants.PackageFlag,
                    $"No subscribers for {name}!", this);
                return;
            }

            if (!string.IsNullOrWhiteSpace(componentName) && !string.IsNullOrWhiteSpace(assetName))
            {
                Utility.Logger.Log(Utility.Logger.Level.INFO,
                    DACKConstants.PackageFlag,
                    $"{name}: Raising event from [{componentName}] using SO [{assetName}] with no data.");
            }
            OnEventRaised.Invoke(null);
        }

        /// <summary>
        /// Raises an event with some data.
        /// </summary>
        /// <param name="data"><see cref="IGameEventData"/> object with information for the event.</param>
        /// <param name="componentName">Optional component name this event is called from for debugging.</param>
        /// <param name="assetName">Optional asset name of the event that's raising the event for debugging.</param>
        public virtual void Raise(IGameEventData data, string componentName = null, string assetName = null)
        {
            if (OnEventRaised == null)
            {
                Utility.Logger.Log(Utility.Logger.Level.WARNING,
                    DACKConstants.PackageFlag,
                    $"No subscribers for {name}!", this);
                return;
            }

            if (!string.IsNullOrWhiteSpace(componentName) && !string.IsNullOrWhiteSpace(assetName))
            {
                Utility.Logger.Log(Utility.Logger.Level.INFO,
                    DACKConstants.PackageFlag,
                    $"{name}: Raising event from [{componentName}] using SO [{assetName}] with [{data.GetType().Name}] data.");
            }
            OnEventRaised.Invoke(data);
        }

        /// <summary>
        /// Helper method to subscribe to this game event with the supplied callback function.
        /// Useful for doing null coalescing for optional events in a game object instead of doing
        /// explicit null checking if subscribing to the UnityAction directly.
        /// </summary>
        /// <param name="func">Callback function that takes an <see cref="IGameEventData"/></param>
        public virtual void Subscribe(UnityAction<IGameEventData> func)
        {
            OnEventRaised += func;
        }

        /// <summary>
        /// Helper method to unsubscribe to this game event with the supplied callback function.
        /// Useful for doing null coalescing for optional events in a game object instead of doing
        /// explicit null checking if unsubscribing to the UnityAction directly.
        /// </summary>
        /// <param name="func">Callback function that takes an <see cref="IGameEventData"/></param>
        public virtual void Unsubscribe(UnityAction<IGameEventData> func)
        {
            OnEventRaised -= func;
        }

        /// <summary>
        /// Helper method used for editor debugging to allow it to raise events for custom events
        /// that require passing information to the callback. Allows values to be changed and sent
        /// in the property window while the game is running. This should be overwritten by inheriting
        /// events to allow for editor testing.
        /// </summary>
        /// <returns>An <see cref="IGameEventData"/> with the properties in the game event.</returns>
        public virtual IGameEventData GetGameEventData()
        {
            return null;
        }
    }
}