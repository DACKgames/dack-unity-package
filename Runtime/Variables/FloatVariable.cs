﻿// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// Modified by David Hsu
// ----------------------------------------------------------------------------

using UnityEngine;

namespace DACK.Variables
{
    [CreateAssetMenu(menuName = "DACK/Variables/FloatVariable")]
    public class FloatVariable : BaseVariable<float>
    {
#if UNITY_EDITOR
        [Multiline]
        public string DeveloperDescription = "";
#endif

        /// <summary>
        /// Sets the variable to the value specified by another variable.
        /// </summary>
        /// <param name="value"><see cref="IntVariable"/> with the value to set this variable.</param>
        /// <param name="obj"><see cref="GameObject"/> that's changing the variable.</param>
        public void SetValue(FloatVariable value, GameObject obj)
        {
            if (CheckAndLogIsOwner(obj))
            {
                Value = value.Value;
            }
        }

        /// <summary>
        /// Sets the variable to the value specified by a float value.
        /// </summary>
        /// <param name="value"><see cref="float"/> value to set this variable.</param>
        /// <param name="obj"><see cref="GameObject"/> that's changing the variable.</param>
        public void SetValue(float value, GameObject obj)
        {
            if (CheckAndLogIsOwner(obj))
            {
                Value = value;
            }
        }

        /// <summary>
        /// Increases the variable by the amount specified.
        /// </summary>
        /// <param name="amount">Amount to increase the variable.</param>
        /// <param name="obj"><see cref="GameObject"/> that's changing the variable.</param>
        public void ApplyChange(float amount, GameObject obj)
        {
            if (CheckAndLogIsOwner(obj))
            {
                Value += amount;
            }
        }

        /// <summary>
        /// Increases the variable by the amount specified by another variable.
        /// </summary>
        /// <param name="amount"><see cref="IntVariable"/> with the value to increase this variable.</param>
        /// <param name="obj"><see cref="GameObject"/> that's changing the variable.</param>
        public void ApplyChange(FloatVariable amount, GameObject obj)
        {
            if (CheckAndLogIsOwner(obj))
            {
                Value += amount.Value;
            }
        }
    }
}