using System;

namespace DACK.Variables
{
    [Serializable]
    public class FloatReference : GenericReference<float, FloatVariable> { }

    [Serializable]
    public class IntReference : GenericReference<int, IntVariable> { }

    [Serializable]
    public class StringReference : GenericReference<string, StringVariable> { }
}
