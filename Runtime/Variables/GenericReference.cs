using System;
using UnityEngine;

namespace DACK.Variables
{
    /// <summary>
    /// A generic reference object used in game object fields to be able to switch between constant or variables for
    /// the field.
    /// </summary>
    /// <typeparam name="T">Type of variable to store and modify.</typeparam>
    /// <typeparam name="V"><see cref="BaseVariable{T}"/> version of the variable type to use as tha variable.</typeparam>
    [Serializable]
    public class GenericReference<T, V> where V : BaseVariable<T>
    {
        // These fields are needed to show up properly in the property inspector with the custom drawer
        [SerializeField] protected bool useConstant = true;
        [SerializeField] protected T constantValue;
        [SerializeField] protected V variable;

        public GenericReference()
        { }

        public GenericReference(T value)
        {
            UseConstant = true;
            ConstantValue = value;
        }

        /// <summary>
        /// Whether or not the reference uses a constant value or a variable.
        /// </summary>
        public bool UseConstant
        {
            get { return useConstant; }
            protected set { useConstant = value; }
        }

        /// <summary>
        /// The constant value of this reference.
        /// </summary>
        public T ConstantValue
        { 
            get { return constantValue; }
            set { constantValue = value; }
        }

        /// <summary>
        /// The variable associated with this reference.
        /// </summary>
        public V Variable
        { 
            get { return variable; }
            protected set { variable = value; }
        }

        /// <summary>
        /// Accessor for getting and setting the value of this reference.
        /// </summary>
        public T Value
        {
            get { return UseConstant ? ConstantValue : Variable.Value; }
            protected set
            {
                if (UseConstant)
                {
                    ConstantValue = value;
                }
                else
                {
                    Variable.Value = value;
                }
            }
        }

        /// <summary>
        /// Sets the owner of the reference. If the reference is a constant, this does nothing.
        /// </summary>
        /// <param name="obj"><see cref="GameObject"/></param>
        public void SetOwner(GameObject obj)
        {
            if (!UseConstant)
            {
                Variable.SetOwner(obj);
            }
        }

        /// <summary>
        /// Sets whether or not the reference should be a constant or a variable.
        /// </summary>
        /// <param name="value">True if the reference uses a constant value, false if using a variable.</param>
        public void SetUseConstant(bool value)
        {
            UseConstant = value;
        }

        /// <summary>
        /// Directly sets the variable used by this reference.
        /// </summary>
        /// <param name="variable">Variable for this reference to use.</param>
        public void SetVariable(V variable)
        {
            Variable = variable;
        }

        /// <summary>
        /// Sets the value of the reference. If using a constant, it will directly change the number.
        /// If using a variable, the GameObject is required to check if it's the owner.
        /// </summary>
        /// <param name="value">New value to set the reference value.</param>
        /// <param name="obj">Optional <see cref="GameObject"/> to check for the owner.</param>
        public void SetValue(T value, GameObject obj = null)
        {
            if (!UseConstant && !Variable.CheckAndLogIsOwner(obj))
            {
                return;
            }

            Value = value;
        }
    }
}