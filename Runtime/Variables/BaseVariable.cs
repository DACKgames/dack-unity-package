using DACK.Common;
using UnityEngine;

namespace DACK.Variables
{
    /// <summary>
    /// Base variable class that contains the option of owning the variable and checking for the owner.
    /// </summary>
    /// <typeparam name="T">Type of data to store in the variable.</typeparam>
    public class BaseVariable<T> : ScriptableObject
    {
        protected GameObject owner = null;

        [SerializeField] protected T value;

        /// <summary>
        /// Raw value of the variable.
        /// </summary>
        public T Value
        {
            get { return value; }
            set { this.value = value; }
        }

        /// <summary>
        /// Returns true if the owner has been set. False otherwise.
        /// </summary>
        public bool HasOwner => owner != null;

        /// <summary>
        /// Sets the instance id as the owner of this variable.
        /// </summary>
        /// <param name="id">Instance id of the object to store.</param>
        public void SetOwner(GameObject obj)
        {
            if (owner != null && owner != obj)
            {
                // If there's already an owner set, log to show the owner is changing.
                Utility.Logger.Log(Utility.Logger.Level.INFO,
                    DACKConstants.PackageFlag, 
                    $"Previous owner of {name} was {owner.name}:{owner.GetInstanceID()}, now setting to {obj.name}:{obj.GetInstanceID()}.");
            }

            owner = obj;
        }

        /// <summary>
        /// Determines whether or not the passed in id matches the owner of this variable.
        /// </summary>
        /// <param name="id">Instance id of the object to check.</param>
        /// <returns>True if the id matches the set owner or there's no owner, false otherwise.</returns>
        public bool IsOwner(int id)
        {
            return !HasOwner || (HasOwner && owner.GetInstanceID() == id);
        }

        /// <summary>
        /// Determines whether or not the passed in game object instance id matches the owner of this variable.
        /// </summary>
        /// <param name="obj"><see cref="GameObject"/> to check.</param>
        /// <returns>True if the id matches the set owner or there's no owner, false otherwise.</returns>
        public bool IsOwner(GameObject obj)
        {
            return !HasOwner || (HasOwner && owner == obj);
        }

        /// <summary>
        /// Helper method to both check if the game object is the owner and logs any errors.
        /// </summary>
        /// <param name="obj"><see cref="GameObject"/> with the instance id to check.</param>
        /// <returns>True if the id matches the set owner or there's no owner, false otherwise.</returns>
        public bool CheckAndLogIsOwner(GameObject obj)
        {
            if (!IsOwner(obj))
            {
                Utility.Logger.Log(Utility.Logger.Level.WARNING,
                    DACKConstants.PackageFlag,
                    $"{obj.name}:{obj.GetInstanceID()} trying to change {name} variable owned by {owner.name}:{owner.GetInstanceID()}");
                return false;
            }

            return true;
        }
    }
}
