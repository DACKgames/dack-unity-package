﻿using UnityEngine;

namespace DACK.Variables
{
    [CreateAssetMenu(menuName = "DACK/Variables/IntVariable")]
    public class IntVariable : BaseVariable<int>
    {
#if UNITY_EDITOR
        [Multiline]
        public string DeveloperDescription = "";
#endif

        /// <summary>
        /// Sets the variable to the value specified by another variable.
        /// </summary>
        /// <param name="value"><see cref="IntVariable"/> with the value to set this variable.</param>
        /// <param name="obj"><see cref="GameObject"/> that's changing the variable.</param>
        public void SetValue(IntVariable value, GameObject obj)
        {
            if (CheckAndLogIsOwner(obj))
            {
                Value = value.Value;
            }
        }

        /// <summary>
        /// Sets the variable to the value specified by an int value.
        /// </summary>
        /// <param name="value"><see cref="int"/> value to set this variable.</param>
        /// <param name="obj"><see cref="GameObject"/> that's changing the variable.</param>
        public void SetValue(int value, GameObject obj)
        {
            if (CheckAndLogIsOwner(obj))
            {
                Value = value;
            }
        }

        /// <summary>
        /// Increases the variable by the amount specified.
        /// </summary>
        /// <param name="amount">Amount to increase the variable.</param>
        /// <param name="obj"><see cref="GameObject"/> that's changing the variable.</param>
        public void ApplyChange(int amount, GameObject obj)
        {
            if (CheckAndLogIsOwner(obj))
            {
                Value += amount;
            }
        }

        /// <summary>
        /// Increases the variable by the amount specified by another variable.
        /// </summary>
        /// <param name="amount"><see cref="IntVariable"/> with the value to increase this variable.</param>
        /// <param name="obj"><see cref="GameObject"/> that's changing the variable.</param>
        public void ApplyChange(IntVariable amount, GameObject obj)
        {
            if (CheckAndLogIsOwner(obj))
            {
                Value += amount.Value;
            }
        }
    }
}