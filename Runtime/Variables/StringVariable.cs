﻿using UnityEngine;

namespace DACK.Variables
{
    [CreateAssetMenu(menuName = "DACK/Variables/StringVariable")]
    public class StringVariable : BaseVariable<string>
    {
#if UNITY_EDITOR
        [Multiline]
        public string DeveloperDescription = "";
#endif

        /// <summary>
        /// Sets the variable to the value specified by another variable.
        /// </summary>
        /// <param name="value"><see cref="StringVariable"/> with the value to set this variable.</param>
        /// <param name="obj"><see cref="GameObject"/> that's changing the variable.</param>
        public void SetValue(StringVariable value, GameObject obj)
        {
            if (CheckAndLogIsOwner(obj))
            {
                Value = value.Value;
            }
        }

        /// <summary>
        /// Sets the variable to the value specified by a string.
        /// </summary>
        /// <param name="value"><see cref="string"/> value to set this variable.</param>
        /// <param name="obj"><see cref="GameObject"/> that's changing the variable.</param>
        public void SetValue(string value, GameObject obj)
        {
            if (CheckAndLogIsOwner(obj))
            {
                Value = value;
            }
        }
    }
}