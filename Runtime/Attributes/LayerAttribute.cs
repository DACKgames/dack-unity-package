using UnityEngine;

namespace DACK.Attributes
{
    /// <summary>
    /// Attribute to select a single layer.
    /// </summary>
    public class LayerAttribute : PropertyAttribute
    {
    }
}
