﻿using System;
using UnityEngine;

namespace DACK.Attributes
{
    /// <summary>
    /// Attribute for hooking into a property field and responding to changes.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class OnChangedCallAttribute : PropertyAttribute
    {
        public string methodName;
        public OnChangedCallAttribute(string methodNameNoArguments)
        {
            methodName = methodNameNoArguments;
        }
    }
}
