﻿using DACK.Common;
using DACK.Event;
using UnityEngine;

namespace DACK.Audio
{
    /// <summary>
    /// Handles playing one shot sound effects in the game that is heard globally and not from a specific 3D source.
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class AudioGlobalSFXPlayer : MonoBehaviour
    {
        /// <summary>
        /// Audio channel that will signal the sfx player to play audio clips.
        /// </summary>
        [SerializeField]
        [Tooltip("Audio channel that will alert the sfx player to play audio clips.")]
        private AudioClipEvent audioEventChannel;

        /// <summary>
        /// Audio source to be used for playing the one shot sfx.
        /// </summary>
        private AudioSource source;

        private void Awake()
        {
            source = GetComponent<AudioSource>();
        }

        private void OnEnable()
        {
            audioEventChannel.OnEventRaised += OnPlayAudio;
        }

        private void OnDisable()
        {
            audioEventChannel.OnEventRaised -= OnPlayAudio;
        }

        /// <summary>
        /// Callback from an event listener with data for the sound to play.
        /// </summary>
        /// <param name="data"><see cref="IGameEventData"/> object with the sound clip.</param>
        public void OnPlayAudio(IGameEventData data)
        {
            var soundData = data as EventData<AudioClip>;
            
            if (data == null)
            {
                Utility.Logger.Log(Utility.Logger.Level.WARNING,
                    DACKConstants.PackageFlag,
                    "AudioSFXPlayer::OnPlayAudio: Audio data passed in is not a valid AudioClipEventData!");
                return;
            }

            if (soundData.Value != null)
            {
                source.PlayOneShot(soundData.Value, source.volume);
            }
        }
    }
}
