﻿using UnityEngine;

namespace DACK.Audio
{
    /// <summary>
    /// Scritable object asset for the background music, composing of the intro and the main loop.
    /// </summary>
    [CreateAssetMenu(menuName = "DACK/BackgroundAudio")]
    public class BackgroundAudioSO : ScriptableObject
    {
        /// <summary>
        /// The beginning part of the background music before it becomes loopable. Can be null.
        /// </summary>
        public AudioClip Intro;

        /// <summary>
        /// The main loop of the background music that will continue looping until stopped.
        /// </summary>
        public AudioClip Loop;
    }
}
