﻿using DACK.Common;
using DACK.Event;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DACK.Audio
{
    /// <summary>
    /// Handles playing background music that needs to be looped or has an intro.
    /// Requires two AudioSources, one for the intro and one for the loop.
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class AudioMusicPlayer : MonoBehaviour
    {
        // Constants that index the audio sources
        private const int IntroSourceIndex = 0;
        private const int LoopSourceIndex = 1;

        [Tooltip("Event channel to listen to for music events.")]
        [SerializeField] private BackgroundMusicEvent musicEvent;

        /// <summary>
        /// Scriptable asset that contains information for the background music.
        /// </summary>
        [Tooltip("Scriptable asset that contains information for the background music.")]
        [SerializeField] private BackgroundAudioSO music;

        /// <summary>
        /// Determines how long it takes for the music to fade in/out.
        /// </summary>
        [Tooltip("Determines how long it takes for the music to fade in/out.")]
        [SerializeField] private float fadeDelay = 1f;

        /// <summary>
        /// How loud the music will be when fading in from 0-1.
        /// </summary>
        [Tooltip("How loud the music will be when fading in from 0-1.")]
        [SerializeField] private float fadeInVolume = 1f;

        /// <summary>
        /// Whether or not the music will automatically start when the scene loads.
        /// </summary>
        [Tooltip("Whether or not the music will automatically start when the scene loads.")]
        [SerializeField] private bool autoStart = false;

        /// <summary>
        /// Audio sources that will play the music.
        /// </summary>
        private List<AudioSource> Sources { get; set; }

        /// <summary>
        /// Local copy of the current running coroutine so it can be stopped if needed.
        /// </summary>
        private Coroutine playbackCoroutine;

        /// <summary>
        /// Called when the component is loaded.
        /// </summary>
        private void Awake()
        {
            Sources = new List<AudioSource>(GetComponents<AudioSource>());

            Sources[IntroSourceIndex].loop = false;
            Sources[LoopSourceIndex].loop = true;

            if (autoStart)
            {
                playbackCoroutine = StartCoroutine(StartMusicSync());
            }
        }

        private void OnEnable()
        {
            musicEvent.OnEventRaised += OnBackgroundMusicEvent;
        }

        private void OnDisable()
        {
            musicEvent.OnEventRaised -= OnBackgroundMusicEvent;
        }

        /// <summary>
        /// Starts playing the music currently in the audio clips.
        /// </summary>
        public void StartMusic()
        {
            // Stop all music first before starting to play
            StopMusic();
            SelectAndPlayMusic(false);
        }

        /// <summary>
        /// Starts the intro music and queues the loop if the intro is provided. Otherwise, starts the loop music.
        /// </summary>
        /// <param name="fadeIn">True if the music will be fading in and to start the music at volume 0, false otherwise.</param>
        private void SelectAndPlayMusic(bool fadeIn)
        {
            // If there's no music set, do nothing.
            if (music == null)
            {
                return;
            }

            if (music.Intro != null)
            {
                if (fadeIn)
                {
                    Sources[IntroSourceIndex].volume = 0;
                }
                Sources[IntroSourceIndex].clip = music.Intro;
                Sources[IntroSourceIndex].Play();

                // Schedule the loop to play immediately after the intro
                Sources[LoopSourceIndex].clip = music.Loop;
                Sources[LoopSourceIndex].PlayScheduled(AudioSettings.dspTime + music.Intro.length);
            }
            else
            {
                if (fadeIn)
                {
                    Sources[LoopSourceIndex].volume = 0;
                }
                Sources[LoopSourceIndex].clip = music.Loop;
                Sources[LoopSourceIndex].Play();
            }
        }

        /// <summary>
        /// Stops the music.
        /// </summary>
        public void StopMusic()
        {
            StopPlaybackCoroutine();
            Sources.ForEach(x => x.Stop());
        }

        /// <summary>
        /// Starts the music by fading it in.
        /// </summary>
        public void FadeInMusic()
        {
            StopPlaybackCoroutine();
            SelectAndPlayMusic(true);
            StartFadeRoutine(true);
        }

        /// <summary>
        /// Stops the music by slowly fading it out, then stopping it once the volume reaches 0.
        /// </summary>
        public void FadeOutMusic()
        {
            StopPlaybackCoroutine();
            StartFadeRoutine(false);
        }

        /// <summary>
        /// Helper method for starting the fade coroutine.
        /// </summary>
        /// <param name="fadeIn">True if the music is to be faded in, false to fade out.</param>
        private void StartFadeRoutine(bool fadeIn)
        {
            playbackCoroutine = StartCoroutine(DoFadeMusic(fadeIn));
        }

        /// <summary>
        /// Helper method to stop any fade in/out coroutines that are running.
        /// </summary>
        private void StopPlaybackCoroutine()
        {
            if (playbackCoroutine != null)
            {
                StopCoroutine(playbackCoroutine);
            }
        }

        #region Event Callback

        /// <summary>
        /// Used by the event listener to parse what to do when a background event is triggered.
        /// </summary>
        /// <param name="data"><see cref="BackgroundMusicEventData"/> with how to play the music.</param>
        public void OnBackgroundMusicEvent(IGameEventData data)
        {
            var musicData = data as BackgroundMusicEventData;
            if (musicData == null)
            {
                Utility.Logger.Log(Utility.Logger.Level.WARNING,
                    DACKConstants.PackageFlag,
                    "AudioMusicPlayer::OnBackgroundMusicEvent: Data isn't a BackgroundMusicEventData type.");
                return;
            }

            // If the use fade is true, that fade in/out the music. If not, then use the start music flag to see
            // whether the music should immeidately start or stop.
            if (musicData.UseFade)
            {
                if (musicData.StartMusic)
                {
                    FadeInMusic();
                }
                else
                {
                    FadeOutMusic();
                }
            }
            else
            {
                if (musicData.StartMusic)
                {
                    StartMusic();
                }
                else
                {
                    StopMusic();
                }
            }
        }

        #endregion

        #region Coroutines

        /// <summary>
        /// Coroutine for auto-starting the music when the scene loads. Note that this is needed because the audio timing
        /// isn't the same as a frame update. It doesn't always update every frame, so trying to play music in the middle of 
        /// the dspTime changing will cause the audio to play too early for the looping part. Need to wait until we get
        /// the latest time update.
        /// </summary>
        /// <returns>None.</returns>
        private IEnumerator StartMusicSync()
        {
            var lastBeatTime = AudioSettings.dspTime;
            var beatTime = AudioSettings.dspTime;
            while (lastBeatTime == beatTime)
            {
                beatTime = AudioSettings.dspTime;
                yield return null;
            }

            StartMusic();
            playbackCoroutine = null;
        }

        /// <summary>
        /// Performs the fade in/out for the music.
        /// </summary>
        /// <param name="fadeIn">True if this is fading in the music, false otherwise.</param>
        /// <returns>Nothing.</returns>
        private IEnumerator DoFadeMusic(bool fadeIn)
        {
            // Sync the audio dsp time
            var lastBeatTime = AudioSettings.dspTime;
            var beatTime = AudioSettings.dspTime;
            while (lastBeatTime == beatTime)
            {
                beatTime = AudioSettings.dspTime;
                yield return null;
            }

            // Defaults to fading in the intro when doing a fade in. The start volume when fading out
            // should match the source. Both source should have the same volume.
            float startVolume = fadeIn ? 0 : Sources[0].volume;
            float endVolume = fadeIn ? fadeInVolume : 0;
            float fadeStartTime = Time.time;

            // Keep looping through every frame and update the volumne until the fade delay time
            while (Time.time - fadeStartTime < fadeDelay)
            {
                var newVolume = Mathf.Lerp(startVolume, endVolume, (Time.time - fadeStartTime) / fadeDelay);

                foreach (var source in Sources)
                {
                    source.volume = newVolume;
                }

                yield return null;
            }

            foreach (var source in Sources)
            {
                source.volume = endVolume;
            }

            playbackCoroutine = null;
        }

        #endregion
    }
}
