﻿using UnityEngine;

namespace DACK.Common
{
    /// <summary>
    /// Game settings for the player, which is loaded from a settings file.
    /// </summary>
    [CreateAssetMenu(menuName = "DACK/GamePreferences")]
    public class GamePreferencesSO : ScriptableObject
    {
        /// <summary>
        /// Master volume for the game.
        /// </summary>
        [Range(0.0001f, 1f)]
        public float masterVolume = 1f;

        /// <summary>
        /// Volume for the music.
        /// </summary>
        [Range(0.0001f, 1f)]
        public float musicVolume = 1f;

        /// <summary>
        /// Volume for the sfx.
        /// </summary>
        [Range(0.0001f, 1f)]
        public float sfxVolume = 1f;
    }
}
