namespace DACK.Common
{
    /// <summary>
    /// Constants used by the DACK package.
    /// </summary>
    public static class DACKConstants
    {
        public const string PackageFlag = "DACKPackage";
    }
}